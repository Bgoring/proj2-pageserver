from flask import Flask, render_template, request
app = Flask(__name__)


@app.route("/")
def index():
    return "UOCIS docker demo!"


@app.route("/*",defaults = {"path": ""}, strict_slashes = False)
@app.route("/<path:path>")
def hello(path):
    ru = request.__dict__
    pullPath = ru["environ"]['REQUEST_URI']
    if "//" in pullPath:
        return render_template("403.html"), 403
    elif "~" in pullPath:
        return render_template("403.html"), 403
    elif ".." in pullPath:
        return render_template("403.html"), 403
    return render_template(pullPath), 200


@app.errorhandler(Exception)
def error_404_disguise(e):
    return render_template("404.html"), 404


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')

